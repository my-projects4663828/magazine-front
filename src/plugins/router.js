import {createRouter, createWebHistory} from "vue-router";

const routes = [
    {
        path: '/',
        component: () => import("@/pages/HomePage.vue")
    },
    {
        path: '/categories/:id',
        component: () => import("@/pages/HomePage.vue")
    },
    {
        path: '/news',
        component: () => import("@/pages/NewsPage.vue")
    },
    {
        path: '/news-info/:newsId',
        component: () => import("@/pages/NewsInfoPage.vue")
    },
    {
        path: '/login',
        component: () => import("@/pages/LoginPage.vue")
    },
    {
        path: '/journal-edit',
        component: () => import("@/pages/EditPage.vue")
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})