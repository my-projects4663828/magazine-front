import axios from "axios";

export default {
    actions: {
        fetchLanguages(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8505/api/languages')
                    .then((response) => {
                        console.log('tillar olindi')

                        let languages = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateLanguages', languages)
                        resolve()
                    })
                    .catch(() => {
                        console.log('tillar olishda xatolik')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateLanguages(state, languages) {
            state.languages = languages
        }
    },
    state: {
        languages: {
            models: [],
            totalItems: 0
        }
    },
    getters: {
        getLanguages(state) {
            return state.languages.models
        }
    }
}