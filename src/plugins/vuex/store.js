import {createStore} from "vuex";
import picture from "@/plugins/vuex/picture";
import media from "@/plugins/vuex/media";
import journal from "@/plugins/vuex/journal";
import news from "@/plugins/vuex/news";
import category from "@/plugins/vuex/category";
import user from "@/plugins/vuex/user";
import language from "@/plugins/vuex/language";
import author from "@/plugins/vuex/author";
import publisher from "@/plugins/vuex/publisher";

export default createStore({
    modules: {
        author,
        category,
        journal,
        language,
        media,
        news,
        picture,
        publisher,
        user
    }
})