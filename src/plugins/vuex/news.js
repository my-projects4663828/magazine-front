import axios from "axios";

export default {
    actions: {
        fetchNewses(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8505/api/news')
                    .then((response) => {
                        console.log('yangiliklar olindi')

                        let news = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateNewses', news)
                        resolve()
                    })
                    .catch(() => {
                        console.log('yangiliklar olishda xatolik')
                        reject()
                    })
            })
        },
        fetchNews(context, newsId) {
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8505/api/news/' + newsId)
                    .then((response) => {
                        context.commit('updateNews', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('yangilik olishda xatolik')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateNewses(state, newses) {
            state.newses = newses
        },
        updateNews(state, news) {
            state.news = news
        }
    },
    state: {
        newses: {
            models: [],
            totalItems: 0
        },
        news: {
            title: '',
            text: '',
            picture: ''
        }
    },
    getters: {
        getNewses(state) {
            return state.newses.models
        },
        getNews(state) {
            return state.news
        }
    }
}