import axios from "axios";

export default {
    actions: {
        fetchJournals(context, data) {
            if (!data.page) {
                data.page = 1
            }

            let url = '?page=' +data.page

            if (data.categoryId !== null) {
                url += '&category=' + data.categoryId
            }
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8505/api/journals' +url)
                    .then((response) => {
                        console.log('jurnallar olindi')

                        let journals = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateJournals', journals)
                        resolve()
                    })
                    .catch(() => {
                        console.log('jurnallar olishda xatolik')
                        reject()
                    })
            })
        },
        pushJournal(context, data) {
            return new Promise((resolve) => {
                axios
                    .post('http://localhost:8505/api/journals', data)
                    .then((response) => {
                        console.log('muvaffaqaiyatli, then() ishladi')
                        console.log(response.data)
                        resolve()
                    })
            })
        },
    },
    mutations: {
        updateJournals(state, journals) {
            state.journals = journals
        }
    },
    state: {
        journals: {
            models: [],
            totalItems: 0
        }
    },
    getters: {
        getJournals(state) {
            return state.journals.models
        },
        getTotalJournals(state) {
            return state.journals.totalItems
        }
    }
}