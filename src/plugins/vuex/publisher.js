import axios from "axios";

export default {
    actions: {
        fetchPublishers(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8505/api/publishers')
                    .then((response) => {
                        console.log('nashriyotlar olindi')

                        let publishers = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updatePublishers', publishers)
                        resolve()
                    })
                    .catch(() => {
                        console.log('nashriyotlar olishda xatolik')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updatePublishers(state, publishers) {
            state.publishers = publishers
        }
    },
    state: {
        publishers: {
            models: [],
            totalItems: 0
        }
    },
    getters: {
        getPublishers(state) {
            return state.publishers.models
        }
    }
}