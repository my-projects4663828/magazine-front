import axios from "axios";

export default {
    actions: {
        fetchAuthors(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('http://localhost:8505/api/authors')
                    .then((response) => {
                        console.log('avtorlar olindi')

                        let authors = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateAuthors', authors)
                        resolve()
                    })
                    .catch(() => {
                        console.log('avtorlar olishda xatolik')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateAuthors(state, authors) {
            state.authors = authors
        }
    },
    state: {
        authors: {
            models: [],
            totalItems: 0
        }
    },
    getters: {
        getAuthors(state) {
            return state.authors.models
        }
    }
}